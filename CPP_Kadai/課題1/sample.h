/*
SampleClassのクラスを作っている

クラスの宣言だけで値を入れるのは18日現在ではしていない
*/

class SampleClass {
	
	//メンバ変数
	int a;
	int b;
	int c;

	//メンバ関数
public://ほかの所でもこの関数を使える
	void Input();
	void Plus();
	void Disp();

};
