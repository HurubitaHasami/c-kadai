#include "Player.h"
#include<iostream>
using namespace std;

Player::Player()
{
	//コンストラクタを使い初期化
	hp = 300;
	atk = 50;
	def = 35;
}
void Player::Disphp()
{
	//hp表示
	cout << "プレイヤーのHP" << hp << "\n";
}

int Player::Attack(int i) {
	cout << "プレイヤーの攻撃！";
	return atk - i / 2;//ダメージ量は 『自分の攻撃力 − 相手の防御力 ÷ ２』 
}

void Player::Damage(int i)
{
	cout << "プレイヤーは" << i << "のダメージ！\n";
	hp = hp - i;
}