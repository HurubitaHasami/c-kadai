/*
クラスのメンバ関数を定義する
*/
#include<iostream>
using namespace std;
#include"CircleClass.h"

//ここでどのように関数を定義するかを決める
void CircleClass::Input() {

	cout << "半径は？";
	cin >> r;//rを入れる


}

void CircleClass::Calc() {
	area = r * r * 3.14f;
}


void CircleClass::Disp() {
	cout << "円の面積＝" << area << "\n";
}