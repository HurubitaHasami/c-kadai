#include<iostream>
#include"Calclation.h"
using namespace std;



//プロトタイプ宣言
void SetX(float a, float b);
void SetY(float a, float b);
//グローバル変数

Calclation * x, * y;
//ポインタは4バイト固定

int main()
{
	//インスタンスXの作成
	x = new Calclation;
	SetX(5.0, 10.0);
	x->Disp();

	delete x;



	//インスタンスＹの処理
	y = new Calclation;
	SetY(8.0, 3.0);
	y->Disp();

	delete y;
	
	
	


}



//インスタンスＸのアクセス関数を呼ぶ
void SetX(float a, float b)
{
	x->SetA(a);//それぞれセッターに変数を入れている
	x->SetB(b);//それぞれセッターに変数を入れている(ポインタなのでアロー演算子を忘れずに）
}
//インスタンスＹのアクセス関数を呼ぶ
void SetY(float a, float b)
{
	y->SetA(a);
	y->SetB(b);
}