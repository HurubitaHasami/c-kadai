#include<iostream>
#include"Player.h"
#include"Enemy.h"
#include"Game.h"
using namespace std;

void Game::GameLoop()
{
	Player play;//プレイヤークラスのインスタンス
	Enemy enemy;//敵クラスのインスタンス

	int damage;//攻撃した時のダメージ

	for (int turn = 1; ;turn++) //forを無限ループしている

	{
		cout << "\n＝＝＝＝＝＝" << turn << "ターン目＝＝＝＝＝＝\n";


		play.Disphp();//それぞれのhpを表示
		enemy.Disphp();//上に同じ

		///////////////////////////////////////////
		//主人公の攻撃
		damage = play.Attack(enemy.GetDefence());
		//damageという変数にplayerが与えたダメージを入れる
		//この際、敵の防御力を使わなければならないため引数はenemy.GetDefence

		enemy.Damage(damage);
		//ひかなければならないため、さっきのdamageを使う

		//戦闘不能判定
		if (enemy.Dead()) break;
		///////////////////////////////////////////
		



		///////////////////////////////////////////
		//敵の攻撃
		damage = enemy.Attack(play.GetDefence());
		//damageという変数にenemyが与えたダメージを入れる
		//この際、引数はプレイヤーの防御力を使って計算する


		play.Damage(damage);
		//ひかなければならないため、さっきのdamageを使う


		//戦闘不能判定
		if (play.Dead()) break;
		///////////////////////////////////////////
	}

	cout << "終了";
}