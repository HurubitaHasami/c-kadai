class Enemy 
{
	int hp, atk, def;

public:
	Enemy();//コンストラクタ
	void Disphp();//hpを表示

	int Attack(int i);//攻撃

	void Damage(int i);//ダメージを受ける

	bool Dead();//戦闘不能になったか判定

	int GetDefence();//ディフェンスを取得

};
