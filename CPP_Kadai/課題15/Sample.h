#pragma once
class Sample
{
protected://これを使うとこのクラスと継承したクラスからアクセスできる！
		//メンバ変数
		int a;
		int b;
		int c;

		//メンバ関数
	public://ほかの所でもこの関数を使える
		void Input();
		void Plus();
		void Disp();


};

